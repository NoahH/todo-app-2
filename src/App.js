import React, { Component } from "react";
import "./index.css";
import TodoList from "./TodoList";
import {
  Route,
  NavLink,
} from "react-router-dom";

class App extends Component {
  state = {
    todos: [],
    inputText: ""
  };
  submitBox = (event) => {
    if(event.key === "Enter" && this.state.inputText !== ""){
      let arr = this.state.todos;
      arr.push({id: 6, userId: 2, title: this.state.inputText, completed: false});
      this.setState({todos: arr, inputText: ""});
      document.getElementsByClassName("new-todo")[0].value = "";
    }
    else{
      this.setState({todos: this.state.todos, inputText: event.target.value});
    }
  }

  toggleCompletion = (e) => {
    let arr = this.state.todos.slice(0, e.target.getAttribute("identifier"));
    let arr2 = this.state.todos.slice(e.target.getAttribute("identifier"), this.state.todos.length);
    arr2.shift();
    let temp = this.state.todos[e.target.getAttribute("identifier")];
    this.setState({todos: arr.concat([{id: temp.id, userId: temp.userId, title: temp.title, completed: !temp.completed}], arr2), inputText: this.state.inputText});
  }

  delete = (e) => {
    let arr = this.state.todos;
    arr.splice(e.target.parentElement.parentElement.getAttribute("Identifier"), 1);
    this.setState({todos: arr,inputText: this.state.inputText})
  }
  clear = () => {
    let arr =[];
    for (let i = 0; i < this.state.todos.length; i ++){
      if(this.state.todos[i].completed !== true)
        arr.push(this.state.todos[i]);
    }
    this.setState({todos: arr, inputText: this.state.inputText});
  }

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autoFocus
            onKeyUp={this.submitBox}
          />
        </header>

        
            <Route 
              exact 
              path="/active"
              render = {() => (
                <TodoList todos={this.state.todos.filter(todo => todo.completed === false)} action = {this.toggleCompletion} delete = {this.delete} whatToRender = "active"/>
              )}>
            </Route>
            <Route 
              exact 
              path="/completed"
              render = {() => (
                <TodoList todos={this.state.todos.filter(todo => todo.completed === true)} action = {this.toggleCompletion} delete = {this.delete} whatToRender = "completed"/>
              )}>
            </Route>
            <Route 
              exact 
              path="/"
              render = {() => (
                <TodoList todos={this.state.todos} action = {this.toggleCompletion} delete = {this.delete} whatToRender = ""/>
              )}>
            </Route>
          

        <footer className="footer">
          <span className="todo-count">
            <strong>
              {(() => {
                let incomplete = this.state.todos.filter(
                  todo => todo.completed !== true
                );
                return incomplete.length;
              })()}
            </strong>{" "}
            item(s) left
          </span>
          <ul className="filters">
            <li>
              <NavLink exact to="/" activeClassName="selected">
                All
              </NavLink>
            </li>
            <li>
              <NavLink exact to="/active" activeClassName="selected">
                Active
              </NavLink>
            </li>
            <li>
              <NavLink exact to="/completed" activeClassName="selected">
                Completed
              </NavLink>
            </li>
          </ul>
          <button className="clear-completed" onClick = {this.clear}>Clear completed</button>
        </footer>
      </section>
    );
  }
}

export default App;
